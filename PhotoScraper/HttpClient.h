//
//  HttpClient.h
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HttpClient : NSObject

- (UIImage*)downloadImage:(NSString*)url;

@end
