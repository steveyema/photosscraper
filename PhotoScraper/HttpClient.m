//
//  HttpClient.m
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "HttpClient.h"

@implementation HttpClient

- (UIImage*)downloadImage:(NSString*)url
{
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    return [UIImage imageWithData:data];
}

@end
