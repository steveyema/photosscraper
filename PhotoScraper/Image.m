//
//  Image.m
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "Image.h"

@implementation Image
{
    UIImageView *imageView;
    UIActivityIndicatorView *indicator;
}

- (id)initWithFrame:(CGRect)frame imageUrl:(NSString *)imageUrl
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
        self.backgroundColor = [UIColor blackColor];
        // the coverImage has a 5 pixels margin from its frame
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, frame.size.width-10, frame.size.height-10)];
        [self addSubview:imageView];
        
        indicator = [[UIActivityIndicatorView alloc] init];
        indicator.center = self.center;
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        [indicator startAnimating];
        [self addSubview:indicator];
    
        [imageView addObserver:self forKeyPath:@"image" options:0 context:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadImageNotification"
                                                            object:self
                                                          userInfo:@{@"imageView":imageView, @"imageUrl":imageUrl}];
    }
    return self;
}

- (void)dealloc
{
    [imageView removeObserver:self forKeyPath:@"image"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"image"])
    {
        [indicator stopAnimating];
    }
}

@end