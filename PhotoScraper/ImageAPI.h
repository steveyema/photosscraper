//
//  ImageAPI.h
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageAPI : NSObject

+(ImageAPI *)sharedInstance;
-(NSArray *)imageDetection:(NSString *)htmlSourceCodeString baseURL:(NSURL *)baseURL;

@end
