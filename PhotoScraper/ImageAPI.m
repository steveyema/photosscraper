//
//  ImageAPI.m
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ImageAPI.h"
#import "HttpClient.h"

@interface ImageAPI()
{
    HttpClient *client;
}

@end

@implementation ImageAPI

-(id)init
{
    if (self = [super init]) {
        client = [[HttpClient alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadImage:) name:@"downloadImageNotification" object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)downloadImage:(NSNotification*)notification
{
    UIImageView *imageView = notification.userInfo[@"imageView"];
    NSString *imageUrl = notification.userInfo[@"imageUrl"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [client downloadImage:imageUrl];
        
        //save to photo album
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);

        dispatch_sync(dispatch_get_main_queue(), ^{
            imageView.image = image;
        });
    });
}

+(ImageAPI *)sharedInstance
{
    static ImageAPI *sharedInstance;
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        sharedInstance = [[ImageAPI alloc] init];
    });
    return sharedInstance;
}

-(NSArray *)imageDetection:(NSString *)htmlSourceCodeString baseURL:(NSURL *)baseURL
{
    NSMutableArray *imageArray = [NSMutableArray new];
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"(<img\\s[\\s\\S]*?src\\s*?=\\s*?['\"](.*?)['\"][\\s\\S]*?>)+?"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    [regex enumerateMatchesInString:htmlSourceCodeString
                            options:0
                              range:NSMakeRange(0, [htmlSourceCodeString length])
                         usingBlock:^(NSTextCheckingResult *result, NSMatchingFlags flags, BOOL *stop) {
                             
                             NSString *img = [htmlSourceCodeString substringWithRange:[result rangeAtIndex:2]];
                             NSLog(@"img src %@",img);
                             
                             NSURL *resolvedURL = [NSURL URLWithString:img relativeToURL:baseURL];
                             [imageArray addObject:resolvedURL];
                         }];
    
    return imageArray;
}

@end
