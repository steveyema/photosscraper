//
//  ImageListTableViewController.m
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ImageListTableViewController.h"
#import "Image.h"

@interface ImageListTableViewController ()
{
    UIActivityIndicatorView *indicator;
    BOOL imageArrayLoaded;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@end

@implementation ImageListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addObserver:self forKeyPath:@"imageArray" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];

}

-(void)dealloc
{
    [self removeObserver:self forKeyPath:@"imageArray"];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"From KVO");
    
    if([keyPath isEqualToString:@"imageArray"])
    {
        imageArrayLoaded = YES;
        [indicator stopAnimating];
        [indicator removeFromSuperview];
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.imageArray.count;
}

-(CGFloat)tableView:(nonnull UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return 100;
}

-(CGFloat)tableView:(nonnull UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    return imageArrayLoaded? 0 : 20;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 20)];
    if (!imageArrayLoaded) {
        indicator = [[UIActivityIndicatorView alloc] init];
        indicator.center = headerView.center;
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [headerView addSubview:indicator];
        [indicator startAnimating];
    }
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageIdentifier" forIndexPath:indexPath];
    
    NSURL *url = self.imageArray[indexPath.row];
    Image *image = [[Image alloc] initWithFrame:CGRectMake(0, 0, 100, 100) imageUrl:url.absoluteString];
    
    [cell.contentView addSubview:image];
    
    return cell;
}

@end
