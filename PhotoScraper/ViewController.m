//
//  ViewController.m
//  PhotoScraper
//
//  Created by Ye Ma on 2015-06-11.
//  Copyright © 2015 Ye Ma. All rights reserved.
//

#import "ViewController.h"
#import "ImageListTableViewController.h"
#import "ImageAPI.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *pageUrl;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.pageUrl becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (self.pageUrl.text.length <= 0) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Invalid URL" preferredStyle:UIAlertControllerStyleAlert];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alertController addAction:okAction];
        return NO;
    }
    else {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.pageUrl.text]]];
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    ImageListTableViewController *tv = (ImageListTableViewController*)[segue destinationViewController];
    
    __weak ViewController *weakself = self;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSURL *baseURL = [NSURL URLWithString:weakself.pageUrl.text];
        
        NSError *error;
        NSString *htmlPage = [NSString stringWithContentsOfURL:baseURL
                                                 encoding:NSASCIIStringEncoding
                                                    error:&error];
        
        if (htmlPage != nil) {
            NSArray *imageArray = [[ImageAPI sharedInstance] imageDetection:htmlPage baseURL:baseURL];

            dispatch_sync(dispatch_get_main_queue(), ^{
                tv.imageArray = imageArray;
            });
        }

    });
}


-(IBAction)unwindToDoList:(UIStoryboardSegue*)segue
{
}

@end
